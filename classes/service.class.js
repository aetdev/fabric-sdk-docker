'use strict';
let path = require('path');
const fs = require('fs-extra');
const util = require('util');
const fabricUtils = require('../utils.js');

const IMAGES = {
    "starter-kit":  {image: "hyperledger/fabric-starter-kit", command: ""},
    "sdk-py":       {image: "hyperledger/fabric-sdk-py", command: ""},
    "membersrvc":   {image: "hyperledger/fabric-membersrvc", command: ""},
    "couchdb":      {image: "hyperledger/fabric-couchdb", command: ""},
    "zookeeper":    {image: "hyperledger/fabric-zookeeper", command: ""},
    "kafka":        {image: "hyperledger/fabric-kafka", command: ""},
    "baseimage":    {image: "hyperledger/fabric-baseimage", command: ""},
    "baseos":       {image: "hyperledger/fabric-baseos", command: ""},
    "basejvm":      {image: "hyperledger/fabric-basejvm", command: ""},
    "ca-peer":      {image: "hyperledger/fabric-ca-peer", command: ""},
    "ca-orderer":   {image: "hyperledger/fabric-ca-orderer", command: ""},
    "ca-tools":     {image: "hyperledger/fabric-ca-tools", command: ""},
    "peer-evm":     {image: "hyperledger/fabric-peer-evm", command: ""},
    "ca":           {image: "hyperledger/fabric-ca", command: "sh -c 'fabric-ca-server start -b admin:adminpw -d'"},
    "peer":         {image: "hyperledger/fabric-peer", command: "peer node start"},
    "orderer":      {image: "hyperledger/fabric-orderer", command: "orderer"},
    "ccenv":        {image: "hyperledger/fabric-ccenv", command: ""},
    "tools":        {image: "hyperledger/fabric-tools", command: ""},
    "client":        {image: "hyperledger/fabric-tools", command: "/bin/bash"},
    "javaenv":      {image: "hyperledger/fabric-javaenv", command: ""}
};

class Service {
    constructor(container_name, image, ports, networks, envs, workingDir, command, volumes, dependsOn, tty, stdinOpen) {

        if (!container_name) {
            throw new Error('Missing container name parameter');
        }

        if (!image) {
            throw new Error('Missing image parameter');
        }

        this.container_name = container_name;
        this.image = IMAGES[image].image;

        if (workingDir) {
            this.working_dir = workingDir;
        }

        if (command) {
            this.command = command;
        } else {
            this.command = IMAGES[image].command;
        }

        if (volumes) {
            this.volumes = volumes;
        }

        if (dependsOn) {
            this.depends_on = Service.makeDependencies(dependsOn);
        }
        
        if (tty) {
            this.tty = tty;
        }

        if (stdinOpen) {
            this.stdin_open = stdinOpen;
        }

        if (envs) {
            this.environment = envs;
        }

        if (ports) {
            this.ports = {};

            if (Array.isArray(ports)) {
                for (let i = 0; i < ports.length; i++) {
                    Object.assign(this.ports, Service.portToObj(ports[i]));
                }
            } else {
                this.ports = Service.portToObj(ports);
            }
        }

        if (networks) {

            this.networks = Service.makeNetworks(networks);
        }
    }

    addDependence(dep) {
        if (!this.hasOwnProperty("depends_on")) {
            this.depends_on = [];
        }

        this.depends_on = this.depends_on.concat(Service.makeDependencies(dep));
    }

    static makeDependencies(dep) {
        if (!dep) {
            throw new Error('Missing dep parameter');
        }

        let res = [];

        if (typeof dep === 'string') {
            res.push(dep);
        } else if (Array.isArray(dep)) {
            res = dep;
        }

        return res;
    }

    addVolume(local_path, docker_path) {
        if (!this.hasOwnProperty("volumes")) {
            this.volumes = {};
        }

        this.volumes[local_path] = docker_path;
    }


    addENV(name, value) {
        if (!this.hasOwnProperty("environment")) {
            this.environment = {};
        }

        this.environment[name] = value;

    }

    addPort(local, docker) {
        if (!this.hasOwnProperty("ports")) {
            this.ports = {};
        }

        this.ports[""+local+""] = docker;
    }

    static portToObj(port) {
        let res = {};

        if (typeof port === 'string') {
            port = port.replace(/\s+/g, '');
            let portArr = port.split(':');
            res[""+portArr[0]+""] = portArr[1];
        } else if (typeof port === 'object') {
            res = port;
        }

        return res;
    }

    addNetwork (network) {
        if (!network) {
            throw new Error('Missing network parameter');
        }

        if (!this.hasOwnProperty("networks")) {
            this.networks = [];
        }

        this.networks = this.networks.concat(Service.makeNetworks(network));
    }

    static makeNetworks(networks) {
        if (!networks) {
            throw new Error('Missing networks parameter');
        }

        let res = [];

        if (typeof networks === 'string') {
            res.push(networks);
        } else if (Array.isArray(networks)) {
            res = networks;
        }

        return res;
    }

    toCompose() {
        let res = {};

        if (this.hasOwnProperty("container_name")) {
            res.container_name = this.container_name;
        }

        if (this.hasOwnProperty("image")) {
            res.image = this.image;
        }

        if (this.hasOwnProperty("networks")) {
            res.networks = this.networks;
        }

        if (this.hasOwnProperty("depends_on")) {
            res.depends_on = this.depends_on;
        }

        if (this.hasOwnProperty("command") && this.command) {
            res.command = this.command;
        }

        if (this.hasOwnProperty("tty") && this.tty) {
            res.tty = this.tty;
        }

        if (this.hasOwnProperty("stdin_open") && this.stdin_open) {
            res.stdin_open = this.stdin_open;
        }

        if (this.hasOwnProperty("working_dir") && this.working_dir) {
            res.working_dir = this.working_dir;
        }

        if (this.hasOwnProperty("environment")) {
            if (Object.keys(this.environment).length) {
                res.environment = [];

                for (let name in this.environment) {
                    if (this.environment.hasOwnProperty(name)) {
                        res.environment.push(name + "=" + this.environment[name]);
                    }
                }
            }
        }

        if (this.hasOwnProperty("ports")) {
            if (Object.keys(this.ports).length) {
                res.ports = [];

                for (let local in this.ports) {
                    if (this.ports.hasOwnProperty(local)) {
                        res.ports.push(local + ":" + this.ports[local]);
                    }
                }
            }
        }

        if (this.hasOwnProperty("volumes")) {
            if (Object.keys(this.volumes).length) {
                res.volumes = [];

                for (let local_path in this.volumes) {
                    if (this.volumes.hasOwnProperty(local_path)) {
                        res.volumes.push(local_path + ":" + this.volumes[local_path]);
                    }
                }
            }
        }

        return res;
    }
}

module.exports = Service;