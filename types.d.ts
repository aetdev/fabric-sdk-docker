declare namespace DockerCompose {
    export class DockerCompose {
        version: number|string;
        volumes?: object;
        networks?: object;
        services?: object;

        constructor(version?: string, volumes?: object, networks?: object, services?: object)

        public addVolume(name: string): void

        public addNetwork(name: string): void

        public addServices(service: Service, name?: string): void

        public toJson(format?: boolean): string;

        public saveToFile(configPath?: string, filename?: string): void

        public static configFileLocation(): string


    }

    export class Service {
        container_name: string;
        image: string;
        working_dir?: string;
        command?: string;
        volumes?: {};
        depends_on?: string[];
        tty?: boolean;
        stdin_open?: boolean;
        environment?: object;
        ports?: object;
        networks?: string[];


        constructor(container_name: string, image: string, ports?: object, networks?: string | [], envs?: object, workingDir?: string, command?: string, volumes?: object, dependsOn?: string | string[], tty?: boolean, stdinOpen?: boolean)

        public addDependence(dep): void

        public static makeDependencies(dep): string[]

        public addVolume(local_path, docker_path): void

        public addENV(name, value): void

        public addPort(local, docker): void

        public static portToObj(port): object

        public addNetwork(network): void

        public static makeNetworks(networks): string[]

        public toCompose()
    }

}

export = DockerCompose;