'use strict';

let exp = {};

const ENV = {
    peer: {
        "CORE_VM_ENDPOINT":                         "unix:///host/var/run/docker.sock",
        "CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE":    "",
        "FABRIC_LOGGING_SPEC":                      "INFO",
        "CORE_PEER_TLS_ENABLED":                    true,
        "CORE_PEER_GOSSIP_USELEADERELECTION":       true,
        "CORE_PEER_GOSSIP_ORGLEADER":               false,
        "CORE_PEER_PROFILE_ENABLED":                true,
        "CORE_PEER_TLS_CERT_FILE":                  "/etc/hyperledger/fabric/tls/server.crt",
        "CORE_PEER_TLS_KEY_FILE":                   "/etc/hyperledger/fabric/tls/server.key",
        "CORE_PEER_TLS_ROOTCERT_FILE":              "/etc/hyperledger/fabric/tls/ca.crt",
        "CORE_PEER_ID":                             "peer0.main.arcelormittal-fabric.test",
        "CORE_PEER_ADDRESS":                        "peer0.main.arcelormittal-fabric.test:7051",
        "CORE_PEER_GOSSIP_EXTERNALENDPOINT":        "peer0.main.arcelormittal-fabric.test:7051",
        "CORE_PEER_LOCALMSPID":                     "MainMSP",
        "CORE_LEDGER_STATE_STATEDATABASE":          "CouchDB",
        "CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS":   "couchdb.peer0.main.arcelormittal-fabric.test:5984",
        "CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME": "",
        "CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD": ""
    },
    orderer: {
        "FABRIC_LOGGING_SPEC":                      "DEBUG",
        "ORDERER_GENERAL_LISTENADDRESS":            "0.0.0.0",
        "ORDERER_GENERAL_GENESISMETHOD":            "file",
        "ORDERER_GENERAL_GENESISFILE":              "/var/hyperledger/orderer/orderer.genesis.block",
        "ORDERER_GENERAL_LOCALMSPID":               "OrdererMSP",
        "ORDERER_GENERAL_LOCALMSPDIR":              "/var/hyperledger/orderer/msp",
        "ORDERER_GENERAL_TLS_ENABLED":              true,
        "ORDERER_GENERAL_TLS_PRIVATEKEY":           "/var/hyperledger/orderer/tls/server.key",
        "ORDERER_GENERAL_TLS_CERTIFICATE":          "/var/hyperledger/orderer/tls/server.crt",
        "ORDERER_GENERAL_TLS_ROOTCAS":              "[/var/hyperledger/orderer/tls/ca.crt]",
        "ORDERER_KAFKA_TOPIC_REPLICATIONFACTOR":    "1",
        "ORDERER_KAFKA_VERBOSE":                    true
    },
    ca: {
        "FABRIC_CA_HOME":                           "/etc/hyperledger/fabric-ca-server",
        "FABRIC_CA_SERVER_TLS_ENABLED":             true,
        "FABRIC_CA_SERVER_CA_NAME":                 "ca.main.arcelormittal-fabric.test",
        "FABRIC_CA_SERVER_TLS_CERTFILE":            "/etc/hyperledger/fabric-ca-server-config/ca.main.arcelormittal-fabric.test-cert.pem",
        "FABRIC_CA_SERVER_TLS_KEYFILE":             "/etc/hyperledger/fabric-ca-server-config/CA_PRIVATE_KEY_FILE",
        "FABRIC_CA_SERVER_CA_CERTFILE":             "/etc/hyperledger/fabric-ca-server-config/ca.main.arcelormittal-fabric.test-cert.pem",
        "FABRIC_CA_SERVER_CA_KEYFILE":              "/etc/hyperledger/fabric-ca-server-config/CA_PRIVATE_KEY_FILE"
    },
    couchdb: {
        "COUCHDB_USER": "",
        "COUCHDB_PASSWORD": ""
    }

};

exp.getServiceENV = function (type) {

};




exports = exp;