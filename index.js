let path = require('path');
const fs = require('fs-extra');
const util = require('util');

const Service = require('./classes/service.class');

const DockerCompose = class {
    constructor(version, volumes, networks, services) {

        this.version = (!version) ? "2" : version;

        if (volumes) {
            this.volumes = volumes;
        }

        if (networks) {
            this.networks = networks;
        }

        if (services) {
            this.services = services;
        }
    }

    addVolume (name) {
        if (!name || typeof name !== 'string') {
            throw new Error('Missing volume name parameter');
        }

        if (!this.hasOwnProperty("volumes")) {
            this.volumes = {};
        }

        this.volumes[name] = null;
    }

    addNetwork (name) {
        if (!name || typeof name !== 'string') {
            throw new Error('Missing network name parameter');
        }

        if (!this.hasOwnProperty("networks")) {
            this.networks = {};
        }

        this.networks[name] = {name: name};
    }

    addServices (service, name) {
        if (!service || !(service instanceof Service)) {
            throw new Error('Missing services parameter');
        }

        if (!this.hasOwnProperty("services")) {
            this.services = {};
        }

        name = (name) ? name : service.container_name;

        this.services[name] = service;
    }

    toJson(format = false) {
        let res = {
            version: this.version,
            volumes: this.volumes,
            networks: this.networks,
            services: {},
        };

        if (Object.keys(this.services).length) {

            for (var name in this.services) {
                if( this.services.hasOwnProperty(name) ) {
                    res.services[name] = this.services[name].toCompose();
                }
            }
        }

        let json = "";

        if (format) {
            json = JSON.stringify(res, null, 2);
        } else {
            json = JSON.stringify(res);
        }

        return json;
    }

    saveToFile(configPath, filename = "compose-network.json") {
        configPath = (configPath) ? configPath : this.constructor.configFileLocation;

        if (!fs.pathExistsSync(configPath)){
            fs.ensureDirSync(configPath);
        }

        fs.writeFileSync(configPath+filename, this.toJson(true));
    }

    static get configFileLocation() {
        return path.join(RootPATH , '../network/docker/network/');
    }
};

module.exports = DockerCompose;
module.exports.Service = Service;